#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <time.h>

void print_array(int *a, int size) {
  int i;
  fprintf(stderr, "[");
  for (i=0; i<size; i++) {
    if (i == size-1) fprintf(stderr, "%d", a[i]);
    else             fprintf(stderr, "%d, ", a[i]);
  }
  fprintf(stderr, "]\n");
}

void swap(int *a, int *b) {
  int tmp;
  tmp = *a;
  *a = *b;
  *b = tmp;
}

struct timespec g_begin_ts;
struct timespec g_end_ts;
void calc_elapsed_time(int begin) {
  float elapsed_time = 0;

  if (begin) {
    // Begin
    clock_gettime(CLOCK_REALTIME, &g_begin_ts);
  } else {
    // End
    clock_gettime(CLOCK_REALTIME, &g_end_ts);

    elapsed_time = ((float)(g_end_ts.tv_sec - g_begin_ts.tv_sec))
                 + ((float)(g_end_ts.tv_nsec - g_begin_ts.tv_nsec)/1000000000);
    fprintf(stderr, "Elapsed time : [%f sec]\n", elapsed_time);
  }
}


uint32_t insert_sort(int *a, int size) {
  int i;
  int j;
  for (i=0; i<size; i++) {
    for (j=i; j>0; j--) {
      if (a[j] < a[j-1]) swap(&a[j], &a[j-1]);
      else break;
    }
  }

  return 0;
}

uint32_t merge_array(int *array, int begin, int mid, int end, int *work_array) {
  int i, j, k;
  
  i = begin;
  j = mid;

  for (k = begin; k < end; k++) {
    if (i < mid && (j >= end || array[i] <= array[j])) {
      work_array[k] = array[i];
      i++;
    } else {
      work_array[k] = array[i];
      j++;
    }
  }

  return 1;
}

uint32_t split_array(int *array, int begin, int end, int *work_array) {
  int mid;

  if (end - begin < 2) return 0;

  mid = (end+begin)/2;
  split_array(array, begin, mid, work_array);
  split_array(array, mid, end, work_array);

  merge_array(work_array, begin, mid, end, array);

  return 1;
}

uint32_t merge_sort(int *array, int size) {
  int *work_array;
  work_array = (int *)malloc(sizeof(int)*size);
  memset(work_array, 0x00, sizeof(int)*size);
  split_array(array, 0, size, work_array);

  return 1;
}

#define ARRAY_SIZE 100000
int main() {
  int i;
  int array[ARRAY_SIZE] = {1, 2, 10, 100, 30, 1, 55, 23, 0, 4};
  for (i=ARRAY_SIZE-1; i>=0; i--) {
    array[ARRAY_SIZE-i-1] = i;
  }

  int *target_array;
  target_array = (int *)malloc(sizeof(array));

  memcpy(target_array, array, sizeof(array));
  fprintf(stderr, "============= Insert Sorting =============\n");
  calc_elapsed_time(1); 
  // print_array(target_array, ARRAY_SIZE);
  insert_sort(target_array, ARRAY_SIZE);
  // print_array(target_array, ARRAY_SIZE);
  calc_elapsed_time(0); 
  fprintf(stderr, "==========================================\n");

  memcpy(target_array, array, sizeof(array));
  fprintf(stderr, "============= Merge Sorting =============\n");
  calc_elapsed_time(1); 
  // print_array(target_array, ARRAY_SIZE);
  insert_sort(target_array, ARRAY_SIZE);
  // print_array(target_array, ARRAY_SIZE);
  calc_elapsed_time(0); 
  fprintf(stderr, "==========================================\n");

  return 1;
}
